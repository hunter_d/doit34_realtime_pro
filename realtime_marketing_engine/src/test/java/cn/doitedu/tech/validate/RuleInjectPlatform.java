package cn.doitedu.tech.validate;

import org.roaringbitmap.RoaringBitmap;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

public class RuleInjectPlatform {
    public static void main(String[] args) throws SQLException, IOException {

        Connection connection = DriverManager.getConnection("jdbc:mysql://doitedu:3306/rtmk", "root", "root");
        PreparedStatement preparedStatement = connection.prepareStatement("insert into rule_resource (rule_param,target_users,tag_init_values,groovy_code) values (?,?,?,?)");

        String rule_param = "{\"event_id\":\"A\",\"cnt\":3}";

        RoaringBitmap target_users_bm = RoaringBitmap.bitmapOf(1, 3,5);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOut = new DataOutputStream(byteArrayOutputStream);
        target_users_bm.serialize(dataOut);

        byte[] target_users_bytes = byteArrayOutputStream.toByteArray();


        HashMap<Integer, Integer> initValuesMap = new HashMap<>();
        initValuesMap.put(1,1);
        initValuesMap.put(5,2);

        ByteArrayOutputStream baout2 = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(baout2);
        objectOutputStream.writeObject(initValuesMap);

        byte[] initValuesMapBytes = baout2.toByteArray();


        String groovCOde = "hahahahah douniwan";

        preparedStatement.setString(1,rule_param);
        preparedStatement.setBytes(2,target_users_bytes);
        preparedStatement.setBytes(3,initValuesMapBytes);
        preparedStatement.setString(4,groovCOde);


        preparedStatement.execute();


    }
}
