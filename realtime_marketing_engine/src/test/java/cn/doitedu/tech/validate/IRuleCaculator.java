package cn.doitedu.tech.validate;

import org.apache.flink.api.common.state.MapState;

import java.util.HashMap;

public interface IRuleCaculator {

    void open(MapState<Integer,Integer> mapState, HashMap<Integer,Integer> initValues);

    void tagCaculate(EventBean eventBean);

    String tagQuery();

}
