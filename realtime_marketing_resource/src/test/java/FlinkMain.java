public class FlinkMain {


    public static void main(String[] args) throws ClassNotFoundException {

        // 比如，在flink的主程序中，通过某种手段，接收到了外面传入的功能类的代码

        String code1 = "public class Function {     " +
                "    public int add(int x, int y){  " +
                "        return x+y;                " +
                "    }                              " +
                "}                                  ";


        String code2 = "public class Function2 {      " +
                "    public int minus(int x, int y){  " +
                "        return x-y;                  " +
                "    }                                " +
                "}                                    ";

        // 直接在代码中，调用 javac 的 api ，对 code1 代码串进行编译，生成.class文件，
        // 然后调用类加载器，将生成好的这个.class文件加载到类路径中来
        // 然后利用反射的手段，获取到这个类的对象，并调用它的方法
        // groovy动态语言



    }


}
