package cn.doitedu.rtmk.java;

public interface Operator {

    int add(int x,int y);

    int sub(int x,int y);

    int min(int x,int y);

}
