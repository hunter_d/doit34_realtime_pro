package cn.doitedu

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.elasticsearch.spark.sql.EsSparkSQL

/**
 * @Author: deep as the sea
 * @Site: <a href="www.51doit.com">多易教育</a>
 * @QQ: 657270652
 * @Date: 2022/9/15
 * @Desc: 学大数据，到多易教育
 *
 * es查询常用命令：
 *   列出所有的index：  curl -X GET http://doitedu:9200/_cat/indices?
 *   指定index，查询所有数据：  curl -X GET http://doitedu:9200/doeusers/_search?pretty
 *   根据id查询： curl -X GET 'http://doitedu:9200/doeusers/_search?q=_id:5'
 *   范围查询： curl -X GET 'http://doitedu:9200/doeusers/_search?q=guid:>4'
 *   简单匹配查询： curl -X POST -H "Content-Type: application/json" doitedu:9200/doeusers/_search?pretty -d '{"query": {"match": {"tg04": "汽车" } }}'
 **/
object UserProfileTagsLoad2Es {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
    conf.set("es.index.auto.create", "true")
    conf.set("es.nodes", "doitedu")
      .set("es.port", "9200")
      .set("es.nodes.wan.only", "true")

    // 创建spark编程入口
    val spark = SparkSession.builder()
      .config(conf)
      .appName("")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()


    // 读hive的画像标签表
    val df = spark.read.table("test.user_profile_test")

    EsSparkSQL.saveToEs(df,"profile_tags3",Map("es.mapping.id" -> "guid"))

    spark.close()
  }
}
